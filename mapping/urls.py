from django.conf.urls import url

from djgeojson.views import GeoJSONLayerView

from . import views
from . import models

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^data.geojson$', GeoJSONLayerView.as_view(model=models.Point,
        geometry_field='coords'),
        name='data'),
]
