# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-11 15:17
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
from django.contrib.postgres.operations import CreateExtension


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        CreateExtension('postgis'),
        migrations.CreateModel(
            name='Point',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='nom')),
                ('coords', django.contrib.gis.db.models.fields.PointField(srid=4326, verbose_name='coordonnées')),
                ('description', models.TextField(blank=True, verbose_name='description')),
                ('add_date', models.DateTimeField(auto_now_add=True, verbose_name='date d’ajout')),
                ('event_date', models.DateField(verbose_name='date de l’évènement')),
                ('status', models.CharField(choices=[('draft', 'brouillon'), ('active', 'actif')], max_length=50, verbose_name='statut')),
                ('event_status', models.CharField(choices=[('out', 'hors du système judiciaire'), ('in', 'dans le système judiciaire')], max_length=50, verbose_name='statut de l’évènement')),
                ('author_ip', models.GenericIPAddressField(null=True, verbose_name='adresse IP source')),
                ('anonymous_submission', models.BooleanField(default=False, verbose_name='soumission anonyme')),
                ('private_message', models.TextField(null=True, verbose_name='message complémentaire anonyme')),
                ('concerned', models.BooleanField(default=False, verbose_name='l’auteur·e est la personne concernée')),
            ],
        ),
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='titre')),
                ('source_name', models.CharField(max_length=100, verbose_name='nom de la source')),
                ('link', models.URLField(null=True, verbose_name='lien de la source')),
                ('image', models.ImageField(null=True, upload_to='sources/')),
                ('source_date', models.DateField(verbose_name='date de la publication')),
                ('point', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mapping.Point')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='nom')),
                ('description', models.TextField(verbose_name='description')),
            ],
        ),
        migrations.AddField(
            model_name='point',
            name='tags',
            field=models.ManyToManyField(to='mapping.Tag'),
        ),
    ]
